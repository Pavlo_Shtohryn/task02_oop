package com.shtohryn;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Scanner;

public class DepartmentOfHouseChemicals {
    static Scanner scanner = new Scanner(System.in);
    private HashSet<Segment> segments = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentOfHouseChemicals that = (DepartmentOfHouseChemicals) o;
        return Objects.equals(segments, that.segments);
    }

    public void addSegment() {
        System.out.println("Enter the segment name: ");
        String name = scanner.nextLine();
        segments.add(new Segment(name));
    }

    public void removeSegment() {
        System.out.println("Enter the segment name: ");
        String name = scanner.nextLine();

        Iterator<Segment> iterator = segments.iterator();
        while (iterator.hasNext()) {
            Segment segment = (Segment) iterator.next();
            if (segment.getSegmentName().equalsIgnoreCase(name)) {
                iterator.remove();
                break;
            }
        }
    }

    public void printAllSegments() {
        for (Segment segment : segments) {
            System.out.println(segment + " ");
        }
    }

    public void clearSegment() {
        System.out.println("Enter segment to remove ");
        String name = scanner.nextLine();
        Iterator<Segment> iterator = segments.iterator();
        while (iterator.hasNext()) {
            Segment segment = (Segment) iterator.next();
            if (segment.getSegmentName().equalsIgnoreCase(name)) {
                segment.clearSegment();
                break;
            }
        }
    }

    public void printAllSegmentProduction() {
        System.out.println("Enter the segment: ");
        String name = scanner.nextLine();
        Iterator<Segment> iterator = segments.iterator();
        while (iterator.hasNext()) {
            Segment segment = (Segment) iterator.next();
            if (segment.getSegmentName().equalsIgnoreCase(name)) {
                segment.printAllProductions();
                break;
            }
        }
    }

    public void addProductionSegment() {
        System.out.println("Enter segment");
        String name = scanner.nextLine();
        Iterator<Segment> iterator = segments.iterator();
        while (iterator.hasNext()) {
            Segment segment = (Segment) iterator.next();
            if (segment.getSegmentName().equalsIgnoreCase(name)) {
                segment.addProduction();
            }
        }
    }

    public void removeProductionForSegment() {
        System.out.println("Enter the brand of production");
        String brand = scanner.nextLine();
        System.out.println("Enter the production's segment : ");
        String name = scanner.nextLine();
        System.out.println("Enter production's weight ");
        int weight = scanner.nextInt();
        Iterator<Segment> iterator = segments.iterator();
        while (iterator.hasNext()) {
            Segment segment = (Segment) iterator.next();
            if (segment.getSegmentName().equalsIgnoreCase(name)) {
                segment.removeProduction();
            }
        }
    }
}
