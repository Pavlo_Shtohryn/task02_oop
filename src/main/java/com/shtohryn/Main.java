package com.shtohryn;

import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DepartmentOfHouseChemicals department = new DepartmentOfHouseChemicals();
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("1 to add segment: ");
            System.out.println("2 to remove segment: ");
            System.out.println("3 to clean segment: ");
            System.out.println("4 to print all segments: ");
            System.out.println("5 to add production to segment ");
            System.out.println("6 to print segments with productions: ");
            System.out.println("7 to remove production from segment : ");
            System.out.println("0 to exit.");
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    department.addSegment();
                    break;
                case "2":
                    department.removeSegment();
                    break;
                case "3":
                    department.clearSegment();
                    break;
                case "4":
                    department.printAllSegments();
                    break;
                case "5":
                    department.addProductionSegment();
                    break;
                case "6":
                    department.printAllSegmentProduction();
                    break;
                case "7":
                    department.removeProductionForSegment();
                    break;
                case "8":
                    break;
                case "9":
                    break;
                case "0":
                    System.exit(0);
                    break;
            }

        } while (true);
    }
}
