package com.shtohryn;

import java.util.*;

public class Production extends Unit {
    private String brand;
    private int price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Production that = (Production) o;
        return price == that.price &&
                Objects.equals(brand, that.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), brand, price);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Production(String name, int weight, String brand, int price) {
        super(name, weight);
        this.brand = brand;
        this.price = price;
    }
}
