package com.shtohryn;

import java.util.Objects;

public class Unit {
    private String name;
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return weight == unit.weight &&
                Objects.equals(name, unit.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight);
    }

    public Unit(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }
}
