package com.shtohryn;

import java.util.*;

public class Segment {
    static Scanner scanner = new Scanner(System.in);
    private List<Production> productionList = new ArrayList<>();
    private String segmentName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Segment segment = (Segment) o;
        return Objects.equals(productionList, segment.productionList) &&
                Objects.equals(segmentName, segment.segmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productionList, segmentName);
    }

    public List<Production> getProductionList() {
        return productionList;
    }

    public void setProductionList(List<Production> productionList) {
        this.productionList = productionList;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public Segment(String segmentName) {
        this.segmentName = segmentName;
    }

    public void addProduction() {
        System.out.println("Enter the name production:");
        String name = scanner.nextLine();
        System.out.println("Enter the production's brand ");
        String brand = scanner.nextLine();
        System.out.println("Enter price:");
        int price = scanner.nextInt();
        System.out.println("Enter production's weight");
        int weight = scanner.nextInt();
        productionList.add(new Production(name, weight, brand, price));
    }

    public void removeProduction() {
        System.out.println("Choice  production");
        String name = scanner.nextLine();
        System.out.println("Enter brand of production ");
        String brand = scanner.nextLine();
        System.out.println("Checking parameters");
        System.out.println("Enter production's weight");
        int weight = scanner.nextInt();
        System.out.println("And  production's  price");
        int price = scanner.nextInt();
        Iterator<Production> iterator = productionList.listIterator();
        while (iterator.hasNext()) {
            Production production = iterator.next();
            if (production.getBrand().equalsIgnoreCase(brand) && production.getName().equalsIgnoreCase(name) &&
                    production.getPrice() == price && production.getWeight() == weight) {
                iterator.remove();
            }
        }
    }

    public void printAllProductions() {
        for (int i = 0; i < productionList.size(); i++) {
            System.out.println(productionList.get(i) + " ");
        }
    }

    public void clearSegment() {
        productionList.clear();
    }
}
